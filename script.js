/* VARIáVEIS */
const image = document.getElementById('image')
image.style.display = 'none'

let photosList = []
let photosTheme = 'landscapes'
let indexPhoto = 0

let currentDate = new Date().toLocaleDateString()
let currentDay = new Date().getDay()
const weekdays = ['Domingo','Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado']

let figcaption = document.querySelector('#caption')
figcaption.innerHTML = `${weekdays[currentDay]}, ${currentDate}`
figcaption.style.display = 'none'

const btnGeo = document.querySelector('#btnGeo')

const arrowLeft = document.querySelector('#arrowLeft')
arrowLeft.style.display = 'none'
const arrowRight = document.querySelector('#arrowRight')
arrowRight.style.display = 'none'

const spinPulse = document.querySelector('#spin')
spinPulse.style.display = 'none'

const latitudeFlorianopolis = '-27.5969'
const longitudeFlorianopolis = '-48.549'

const camera = document.querySelector('#cam')

const subBackground = document.querySelector('#sub-back')
subBackground.style.display = 'none'



function setImgURL (photoObj) {

    return "https://farm" + photoObj.farm +
            ".staticflickr.com/" + photoObj.server +
            "/" + photoObj.id + "_" + photoObj.secret + ".jpg";
};

function getImageFromFlickrAPI ({lat, long, isDefault}) {

    let latitude = lat;
    let longitude = long;

    if (isDefault) {

        const latitudeFlorianopolis = '-27.5969';
        const longitudeFlorianopolis = '-48.549';
        const florianopolisBeachs = 'beachs'

        photosTheme = florianopolisBeachs
        latitude = latitudeFlorianopolis;
        longitude = longitudeFlorianopolis;
    }

    fetch(`https://shrouded-mountain-15003.herokuapp.com/https://flickr.com/services/rest/?api_key=aefbd7ed9de210b73e9acd7aed4fec99&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=12&lat=${latitude}&lon=${longitude}&text=${photosTheme}`)
    .then((res) => res.json())
    .then((photoObj) => {

        photosList = photoObj.photos.photo
        let firstPhoto = photosList[indexPhoto]

        console.log(firstPhoto)

        if (firstPhoto !== undefined) {

            image.setAttribute('src', setImgURL(firstPhoto))
        }
        else {

            getGeo()
        };
    })
};

function getLatitudeLongitude (position) {

    let latitude = position.coords.latitude
    let longitude = position.coords.longitude

    getImageFromFlickrAPI({lat: latitude, long: longitude})
};

function getGeo() {
    
    navigator.geolocation.getCurrentPosition(getLatitudeLongitude, getImageFromFlickrAPI({isDefault: true}));

    btnGeo.style.display = 'none'
    spinPulse.style.display = 'block'
    camera.style.color = 'whitesmoke'
    camera.style.fontSize = '1.8em'
    subBackground.style.display = 'block'

    setTimeout(() => {
        arrowLeft.style.display = 'block'
        arrowRight.style.display = 'block'
        spinPulse.style.display = 'none'
        image.style.display = 'block'
        figcaption.style.display = 'block'
    }, 450*10);
};

function nextPhoto () {

    indexPhoto++
    let isLastIndex = indexPhoto > photosList.length - 1 ? true : false

    if (isLastIndex) {

        indexPhoto = 0
        image.setAttribute('src', setImgURL(photosList[indexPhoto]))
    }

    image.setAttribute('src', setImgURL(photosList[indexPhoto]))
};

function previousPhoto () {

    indexPhoto--
    let isFirstIndex = indexPhoto < 0 ? true : false

    if (isFirstIndex) {

        indexPhoto = photosList.length - 1
        image.setAttribute('src', setImgURL(photosList[indexPhoto]))
    }

    image.setAttribute('src', setImgURL(photosList[indexPhoto]))
};

btnGeo.addEventListener('click', getGeo);
arrowLeft.addEventListener('click', previousPhoto);
arrowRight.addEventListener('click', nextPhoto);

